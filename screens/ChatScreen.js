
import React from 'react';
import {SafeAreaView ,View, Text, Image, TextInput, Dimensions, TouchableOpacity } from 'react-native';
import styles  from '../constants/styles';
import User from '../User';
import firebase  from 'react-native-firebase';
import { FlatList } from 'react-native-gesture-handler';

var ImagePicker = require('react-native-image-picker');

// More info on all the options is below in the README...just some common use cases shown here
var options = {
  title: 'Select Avatar',
  customButtons: [
    {name: 'fb', title: 'Choose Photo from Facebook'},
  ],
  storageOptions: {
    skipBackup: true,
    path: 'images'
  }
};
// Prepare Blob support
const Blob = RNFetchBlob.polyfill.Blob
const fs = RNFetchBlob.fs
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest
window.Blob = Blob

export default class ChatScreen extends React.Component {
    static navigationOptions = ({navigation}) => {
        return {
            title: navigation.getParam('name',null),
            headerRight: (
                <TouchableOpacity onPress={() => navigation.openDrawer()}>
                    <Image  source={require('../images/settings.png')} style={{ width:30,height:30,marginRight: 7 }} />
                </TouchableOpacity>
            )
        }
    }

    constructor(props){
        super(props);
        this.state = {
            person: {
                name: props.navigation.getParam('name'),
                phone: props.navigation.getParam('phone'),
            },
            //phoneAnterior: '',
            textMessage: '',
            messagesList: [/*{
                message: "111111111111111",
                time: new Date,
                from: "111111",
                name: "pedro"
            },{
                message: "111111111111",
                time: new Date,
                from: "111111",
                name: "pedro"
            },{
                message: "2222222222222222",
                time: new Date,
                from: "222222",
                name: "jose"
            },{
                message: "111111111111111",
                time: new Date,
                from: "111111",
                name: "pedro"
            }*/
        ]

        }
    }

    phoneAnterior = '';


    componentWillMount(){
        //quitarlo de aqui
        firebase.database().ref('messages').child(User.phone).child(this.state.person.phone)
        .on('child_added',(value)=> {
            this.setState((prevState) => {
                return {
                    messagesList: [...prevState.messagesList,value.val()]
                }
            })
        });
    }

    
    /*component() {
        firebase.database().ref('messages').child(User.phone).child(this.state.person.phone)
        .on('child_added',(value)=> {
            this.setState((prevState) => {
                return {
                    messagesList: [...prevState.messagesList,value.val()]
                }
            })
        })
    }*/


    

    convertTime = (time) => {
        let d = new Date(time);
        let c = new Date();
         let result = (d.getHours() < 10 ? '0' : '') + d.getHours() + ':';
         result += (d.getMinutes() < 10 ? '0' : '' ) + d.getMinutes();
         if(c.getDay() !== d.getDay()){
             result = d.getDay() + ' ' + d.getMonth() + ' ' + result;
         }
         return result;
    }

    handleChange = key => val => {
        this.setState({ [key] : val })
    }

    sendMessage = async () => {
        
        if(this.state.textMessage.length > 0 ){
           
            let msgId = firebase.database().ref('messages').child(User.phone).child(this.state.person.phone).push().key
            //alert(msgId)
            let updates = {}
            
            let message = {
                message: this.state.textMessage,
                time: firebase.database.ServerValue.TIMESTAMP,
                from: User.phone,
                name: User.name
            }
            updates['messages/'+User.phone+'/'+this.state.person.phone+'/'+msgId] = message
            updates['messages/${this.state.person.phone}/${User.phone}/${msgId}'] = message
            //firebase.database().ref().update(updates);
            firebase.database().ref().child('messages/'+User.phone+'/'+this.state.person.phone+'/'+msgId).update(message);
            firebase.database().ref().child('messages/'+this.state.person.phone+'/'+User.phone+'/'+msgId).update(message);
            this.setState({ textMessage : ''})
        }
    }

    uploadImage(uri, mime = 'application/octet-stream') {
        return new Promise((resolve, reject) => {
          const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri
          let uploadBlob = null
    
          const imageRef = firebase.storage().ref('images').child('image_001')
    
          fs.readFile(uploadUri, 'base64')
            .then((data) => {
              return Blob.build(data, { type: `${mime};BASE64` })
            })
            .then((blob) => {
              uploadBlob = blob
              return imageRef.put(blob, { contentType: mime })
            })
            .then(() => {
              uploadBlob.close()
              return imageRef.getDownloadURL()
            })
            .then((url) => {
              resolve(url)
            })
            .catch((error) => {
              reject(error)
          })
        })
    }

    //seleccionar Imagen
    selectedImagen(){

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);
      
            if (response.didCancel) {
              console.log('User cancelled image picker');
            }
            else if (response.error) {
              console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
              console.log('User tapped custom button: ', response.customButton);
            }
            else {
              // let source = { uri: response.uri };
              // this.setState({image_uri: response.uri})
      
              // You can also display the image using data:
              // let image_uri = { uri: 'data:image/jpeg;base64,' + response.data };
      
            this.uploadImage(response.uri)
              .then(url => { alert('uploaded'); this.setState({image_uri: url}) })
              .catch(error => console.log(error))
      
            }
          });

    }


    renderUserFrom = (item) => {
        this.phoneAnterior = item.from;
        return (
            <Text style={{ color: '#000', padding: 7, fontSize: 12 }}>
                {item.name}
            </Text>
        )
    }

    renderUser = (item) => {
        this.phoneAnterior = item.from;
        return (
            <Text style={{ color: '#fff', padding: 7, fontSize: 16 }}>
                {item.message}
            </Text>
        )
    }

    renderLastTime = (item,index) => {
        
        if(this.state.messagesList.length <= index + 1){
            return (
                <Text style={{
                    color:'#000',
                    padding: 3,
                    fontSize: 12,
                    alignSelf: item.from === User.phone ? 'flex-end' : 'flex-start',
                    }}
                >
                    {this.convertTime(item.time)} 
                </Text>
            )
        }else{
            if(this.state.messagesList[index+1].from !== item.from){
                return (
                <Text style={{
                    color:'#000',
                    padding: 3,
                    fontSize: 12,
                    alignSelf: item.from === User.phone ? 'flex-end' : 'flex-start',
                    }}
                >
                    {this.convertTime(item.time)} 
                </Text>)
            }
        }
       
    }


    renderRow = ({ item , index }) => {
        return(
            <View>
                { this.phoneAnterior !== item.from && item.from !== User.phone ? this.renderUserFrom(item) : null}
                
                <View style={{
                    flexDirection: 'row',
                    width: '60%',
                    alignSelf: item.from === User.phone ? 'flex-end' : 'flex-start',
                    backgroundColor: item.from === User.phone ? '#00987b' : '#7cb342',
                    borderRadius: 5,
                    marginBottom: 5,
                }}>
                    {this.renderUser(item)}
                    
                </View>
                {this.renderLastTime(item,index)}
            </View>

            
        )
        
    }

    render() {
        let {height,width} = Dimensions.get('window');  
        
        return (
        <SafeAreaView>
            <FlatList 
                style={{padding: 10,height: (height-70) * 0.8 }}
                data={this.state.messagesList}
                renderItem={this.renderRow}
                keyExtractor={(item,index)=>index.toString()}
            />
            <View style={styles.send} >
                <TextInput
                    style={styles.input}
                    value={this.state.textMessage}
                    placeholder="escribe mensage..."
                    onChangeText={this.handleChange('textMessage')}
                />
                <TouchableOpacity onPress={this.sendMessage} style={{padding: 10,paddingBottom: 15,}} >
                    <Image  source={require('../images/ic_menu_send.png')} style={{ width:64,height:45 }} />
                </TouchableOpacity>  
                <TouchableOpacity onPress={this.selectedImagen} style={{padding: 10,paddingBottom: 15,}} >
                    <Image  source={require('../images/ic_menu_send.png')} style={{ width:64,height:45 }} />
                </TouchableOpacity>    
            </View>
            
        </SafeAreaView>
        )
  }
}
