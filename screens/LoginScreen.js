
import React from 'react';
import { Text ,Alert ,AsyncStorage,TouchableOpacity,TextInput, View} from 'react-native';
import User from '../User';
import styles from '../constants/styles';
import firebase from 'react-native-firebase';

export default class LoginScreen extends React.Component {
  static navigationOptions () {
    return {
       header: null
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      phone: '',
      name: ''
    }
  }
  

  
  handleChange ( key) {
    return val => {
      this.setState({ [key]: val });
    };
  }

  submitForm = async () => {
    if(this.state.phone.length < 6){
      Alert.alert('Error','Wrong phone number')
    }else if(this.state.name.length < 3){
      Alert.alert('Error','Wrong name')
    }else{
      ///save user data
      await AsyncStorage.setItem('userPhone',this.state.phone);
      await AsyncStorage.setItem('userName',this.state.name);
      User.phone = this.state.phone;
      User.name = this.state.name;
      firebase.database().ref('users/' + User.phone).set({name: this.state.name});
      this.props.navigation.navigate('App');
    }
    
  }
  
  render() {
    return (
      <View style={styles.container}>
        <TextInput 
          placeholder="Phone number"
          keyboardType="number-pad"
          style={styles.input}
          value={this.state.phone}
          onChangeText={this.handleChange('phone')}
        />
        <TextInput 
          placeholder="Name"
          style={styles.input}
          value={this.state.name}
          onChangeText={this.handleChange('name')}
        />
        <TouchableOpacity onPress={this.submitForm}>
          <Text style={styles.btnText}>Entrar</Text>
        </TouchableOpacity>
      </View>
    );
  }
}


