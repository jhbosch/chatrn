/* jshint esversion: 6 */
import React from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  StatusBar,
  View,
} from 'react-native';
import User from '../User';
import firebase from 'firebase';

export default class AuthLoadingScreen extends React.Component {
  constructor(props) {
    super(props);
    this._bootstrapAsync();
  }

  componentWillMount() {

    /*var firebaseConfig = {
      apiKey: "AIzaSyCWalV6f8a-y9etwnTAp4ehbGGokjRz2IM",
      authDomain: "rnup-5173d.firebaseapp.com",
      databaseURL: "https://rnup-5173d.firebaseio.com",
      projectId: "rnup-5173d",
      storageBucket: "rnup-5173d.appspot.com",
      messagingSenderId: "287202721720",
      appId: "1:287202721720:web:7aff35fe828ad4ab"
    };
    
    firebase.initializeApp(firebaseConfig);*/

  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    User.phone = await AsyncStorage.getItem('userPhone');
    User.name = await AsyncStorage.getItem('userName');

    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    this.props.navigation.navigate(User.phone ? 'App' : 'Auth');
  };

  // Render any loading content that you like here
  render() {
    return (
      <View>
        <ActivityIndicator />
        <StatusBar barStyle="default"  />
      </View>
    );
  }
}