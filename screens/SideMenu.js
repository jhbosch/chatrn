import PropTypes from 'prop-types';
import React from 'react';
import styles from '../constants/styles';
import {NavigationActions} from 'react-navigation';
import {ScrollView, Text, View, AsyncStorage, TouchableOpacity} from 'react-native';
import { StackNavigator } from 'react-navigation';


class SideMenu extends React.Component {
  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  }

  _logOut = async () => {
    await AsyncStorage.clear();
    this.props.navigation.navigate('Auth');
  }

  render () {
    return (
      <View style={styles.containerSideMenu}>
        <ScrollView >
            <View >
                <Text style={styles.sectionHeadingStyle} onPress={()=>this.props.navigation.closeDrawer()}>
                    Settings
                </Text>
                <View style={styles.navSectionStyle}>
                    <TouchableOpacity 
                        //onPress={()=>alert("No implementado aun")}Home
                        onPress={()=>this.props.navigation.navigate("Home")}
                        style={styles.btnText} >
                        <Text style={styles.navItemStyle}>Crear grupo</Text>
                    </TouchableOpacity>
                    <TouchableOpacity 
                        onPress={()=>this.props.navigation.navigate("Camara")}
                        style={styles.btnText} >
                        <Text style={styles.navItemStyle}>Camara</Text>
                    </TouchableOpacity>
                    <TouchableOpacity 
                        onPress={this._logOut}
                        style={styles.btnText} >
                        <Text style={styles.navItemStyle}>Salir</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </ScrollView>
        <View style={styles.footerContainer}>
          <Text>Copyrigth</Text>
        </View>
      </View>
    );
  }
}

SideMenu.propTypes = {
  navigation: PropTypes.object
};

export default SideMenu;