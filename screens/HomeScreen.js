import React from 'react';
import { SafeAreaView, Image, Text ,FlatList ,TouchableOpacity,Alert } from 'react-native';
import User from '../User';
import styles from '../constants/styles';
import firebase  from 'react-native-firebase';

export default class HomeScreen extends React.Component {
    
    static navigationOptions = ({navigation}) => {
        return {
            title: 'Chat',
            headerRight: (
                <TouchableOpacity onPress={() => navigation.openDrawer()}>
                    <Image  source={require('../images/settings.png')} style={{ width:30,height:30,marginRight: 7 }} />
                </TouchableOpacity>
            )
        }
    }

    constructor(props) {
        super(props)
        this.state = {
            users: [/*{
                name: "pedro",
                phone: "111111"
            },{
                name: "jose",
                phone: "222222"
            }*/]
        }
    }
     
    
    

    componentWillMount() {
        let bdRef = firebase.database().ref("users");
        bdRef.on('child_added', (val) => {
            let person = val.val()
            person.phone = val.key;
            if(person.phone !== User.phone){
                this.setState((prevState)=>{
                    return { 
                        users: [...prevState.users,person]
                    }
                })
            }
            
        }) 
            
    }


    renderRow = ({item}) => {
        return(
            <TouchableOpacity 
                onPress={()=>this.props.navigation.navigate('Chat',item)}
                style={{ padding:10,borderBottomColor:'#ccc',borderBottomWidth:1}}>
                <Text style={{fontSize: 20}}>{item.name}</Text>
            </TouchableOpacity>
        )
    }
    
    render(){
        return(
            <SafeAreaView>
                <FlatList 
                    data={this.state.users}
                    renderItem={this.renderRow}
                    keyExtractor={(item) => item.phone}
                />
                
            </SafeAreaView>
        )
        
    }
}