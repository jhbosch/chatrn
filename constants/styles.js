import React from 'react';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },
    input: {
      padding: 10,
      borderWidth: 1,
      borderColor: '#ccc',
      width:'80%',
      marginBottom: 10,
      borderRadius: 5,
  
    },
    btnText: {
      color: 'darkblue',
      fontSize: 20,
    },
    navItemStyle: {
      fontSize: 20,
      paddingBottom: 5,
      paddingLeft: 10,
      paddingTop: 20,
      borderBottomColor: '#ccc',
      borderBottomWidth: 1
    },
    navSectionStyle: {
      //backgroundColor: 'lightgrey'
    },
    sectionHeadingStyle: {
      paddingVertical: 10,
      paddingHorizontal: 5,
      backgroundColor: 'lightgrey',
      fontSize: 25,
      borderBottomColor: '#ccc',
      borderBottomWidth: 2,
      padding: 10,
      textAlign: 'center'
    },
    footerContainer: {
      padding: 20,
      backgroundColor: 'lightgrey',
      borderTopColor: '#ccc',
      borderTopWidth: 1,
    },
    containerSideMenu: {
      //paddingTop: 20,
      flex: 1
    },
    send: {
      flexDirection: 'row',
      alignItems: 'center',
      paddingHorizontal: 10,
      marginBottom: 20,
      borderTopColor: '#ccc',
      borderTopWidth: 3,
      paddingTop: 20
    }
  });

  export default styles;
