import React from 'react';
import { createSwitchNavigator, createStackNavigator, createAppContainer, createDrawerNavigator } from 'react-navigation';
import { Dimensions } from 'react-native';
import LoginScreen from './screens/LoginScreen';
import HomeScreen from './screens/HomeScreen';
import AuthLoadingScreen from './screens/AuthLoadingScreen';
import ChatScreen from './screens/ChatScreen';
import SideMenu from './screens/SideMenu';
import { Provider } from 'react-redux';
import store  from './store';
import Camera from './screens/Camera';

const AppStack = createStackNavigator({ Home: HomeScreen, Chat: ChatScreen,Camara: Camera });
const AuthStack = createStackNavigator({ Login: LoginScreen });
const AppDrawer = createDrawerNavigator({Item: AppStack  },{contentComponent: SideMenu, drawerWidth: Dimensions.get('window').width - 120})

const Navegation = createAppContainer(createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: AppDrawer,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
  }
));


export default class App extends React.Component {
  
  render() {
    console.log(store.getState())
    return (   
      <Provider store={store}>
        <Navegation />
      </Provider>   
          
    )
  }
} 
